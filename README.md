# File Setup Pipeline

![release badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/file-setup-pipeline/-/badges/release.svg)
![pipeline badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/file-setup-pipeline/badges/main/pipeline.svg?ignore_skipped=true)

This software implements a parallelized pipeline to setup image files. It is used for nopaque's File setup service but you can also use it standalone, for that purpose a convenient wrapper script is provided. The pipeline is designed to run on Linux operating systems, but with some tweaks it should also run on Windows with WSL installed.

## Software used in this pipeline implementation

- Official Debian Docker image (buster-slim): https://hub.docker.com/_/debian
  - Software from Debian Buster's free repositories

## Installation

1. Install Docker and Python 3.
2. Clone this repository: `git clone https://gitlab.ub.uni-bielefeld.de/sfb1288inf/file-setup-pipeline.git`
3. Build the Docker image: `docker build -t file-setup-pipeline:latest file-setup-pipeline`
4. Add the wrapper script (`wrapper/file-setup-pipeline` relative to this README file) to your `${PATH}`.
5. Create working directories for the pipeline: `mkdir -p /<my_data_location>/{input,output}`.

## Use the Pipeline

1. Place your images files inside a subdirectory in `/<my_data_location>/input`. It should look similar to this:
```
.
|-- input
|   |-- book
|       |-- page-1.png
|       |-- page-2.png
|       |-- ...
|       `-- page-x.png
`-- output
```
2. Clear your `/<my_data_location>/output` directory.
3. Start the pipeline process. Check the pipeline help (`file-setup-pipeline --help`) for more details.
```bash
cd /<my_data_location>
file-setup-pipeline \
  --input-dir input \
  --output-dir output
```
4. Check your results in the `/<my_data_location>/output` directory.
