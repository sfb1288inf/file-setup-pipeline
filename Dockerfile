FROM debian:buster-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


ENV LANG=C.UTF-8


RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
      ca-certificates \
      imagemagick \
      python2.7 \
      python3.7 \
      wget \
 && rm -r /var/lib/apt/lists/* \
 && mv /etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml.bak


ENV PYFLOW_VERSION=1.1.20
RUN wget --quiet "https://github.com/Illumina/pyflow/releases/download/v${PYFLOW_VERSION}/pyflow-${PYFLOW_VERSION}.tar.gz" \
 && tar -xzf "pyflow-${PYFLOW_VERSION}.tar.gz" \
 && cd "pyflow-${PYFLOW_VERSION}" \
 && python2.7 setup.py build install \
 && cd - > /dev/null \
 && rm -r "pyflow-${PYFLOW_VERSION}" "pyflow-${PYFLOW_VERSION}.tar.gz"


RUN groupadd \
      --system generic-group \
 && useradd \
      --create-home \
      --gid generic-group \
      --no-log-init \
      --system generic-user
USER generic-user
WORKDIR /home/generic-user


COPY file-setup-pipeline /usr/local/bin/


ENTRYPOINT ["file-setup-pipeline"]
CMD ["--help"]
